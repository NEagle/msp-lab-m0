## Example Summary

Toggles One GPIO pin using HW toggle register.

## Peripherals & Pin Assignments

| Peripheral | Pin | Function |
| --- | --- | --- |
| GPIOA | PA0 | Standard Output-LED |
| GPIOA | PA22 | UART RX on CoreBoard |
| GPIOA | PA23 | UART TX on CoreBoard |
| GPIOA | PA10 | OLED SDA PIN(J1) |
| GPIOA | PA11 | OLED SCL PIN(J1) |
| SYSCTL | --- | --- |
| EVENT | --- | --- |

## Example Usage
Compile, load and run the example.  
OLED Pannel on BaseBoard will Display different char.  

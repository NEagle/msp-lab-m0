## Example Summary

Toggles One GPIO pin using HW toggle register.

## Peripherals & Pin Assignments

| Peripheral | Pin | Function |
| --- | --- | --- |
| GPIOA | PA0 | Standard Output-LED |
| GPIOA | PA11 | UART RX on CoreBoard |
| GPIOA | PA10 | UART TX on CoreBoard |
| GPIOA | PA28 | GPIO as OLED SDA PIN(J7) |
| GPIOA | PA31 | GPIO as OLED SCL PIN(J7) |
| SYSCTL | --- | --- |
| EVENT | --- | --- |

## Example Usage
Compile, load and run the example.  
OLED Pannel on BaseBoard will Display different char.  

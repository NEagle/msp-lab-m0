## Example Summary

Led working between 3 Status: Off/On/Flashing while Pressed once Key.

## Peripherals & Pin Assignments

| Peripheral | Pin | Function |
| --- | --- | --- |
| GPIOA | PA0 | Standard Output-LED |
| GPIOA | PA14 | Connect to One Key |
| SYSCTL | --- | --- |
| EVENT | --- | --- |

## Example Usage
Compile, load and run the example.
LED will change status in Three Type: Off/On/Flashing due to Key Pressed.

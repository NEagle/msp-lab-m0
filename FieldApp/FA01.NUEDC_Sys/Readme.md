# 电赛G350x最小系统

## 设计最小系统特征

1. 集成MCU：TI Cortex-M0+ MSPM0G350x，最高80MHz，最大128KByte FLASH  
2. 0.96寸 128x64 OLED屏  
3. 四个用户按键，其中一个为BSL按键，外加一个RESET按键  
4. 三个LED，分别为红色、黄色、白色  
5. 两位拨码开关，提供工作模式选择  
6. 一个2.7KHz的有源蜂鸣器  

## 洞洞板设计特征

1. 集成两个SOP-8转DIP的焊接位置，可应用于多数运放OPA  
2. 集成两个SOT23-5转SOP的焊接位置，可应用于多少电源芯片  
3. 提供GND、+3.3V、VIN等电源焊接位置  
4. 提供11x30(2.54mm)的洞洞板面积  

## 系统原理图及PCB

[电赛MSPM0G350x最小系统原理图](https://gitee.com/NEagle/msp-lab-m0/raw/master/FieldApp/FA01.NUEDC_Sys/Hardware/SCH_%E7%94%B5%E8%B5%9B%E6%9C%80%E5%B0%8F%E7%B3%BB%E7%BB%9F.pdf "原理图")  

![电赛MSPM0G350x最小系统PCB版图正面](./Hardware/Top.png "PCB正面")
![电赛MSPM0G350x最小系统PCB版图反面](./Hardware/Bot.png "PCB反面")

## BaseCode基础代码[目录](./BaseCode/)

1. 基础代码将板卡已经布线的各个外设均做了应用  
2. 四个用户按键将不断切换OLED上的字符的显示方式，阳文/阴文不断切换  
3. 两个拨码开关，当ON状态显示阴文，OFF状态显示阳文  
4. 当Key1持续按下，则蜂鸣器响  
5. 三个LED组成3bit，按照0.5秒的间隔依次组合显示  
6. 基础代码采用时间片轮询方式实现，以10ms作为一个时间片的颗粒度  
7. 除应用任务外，集成了printf函数，打印在UART转USB口  
8. 系统有两个不同时间间隔的打印函数，不断在UART口打印  
9. OLED支持中英文显示，字库工具参考[链接](https://pan.baidu.com/s/1E55LQukfLvL84VKnJsmBwg?pwd=3msj)


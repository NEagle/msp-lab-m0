/*
 * task_main.c
 *
 *  Created on: 2024年1月21日
 *      Author: junying
 */

#include "task_main.h"
#include "printf.h"     //User Defined Embeded printf Func
#include "uart.h"
#include "Hardware.h"

//每秒UART打印任务
void TaskUartPntPer1S(void);

//每3秒UART打印任务
void TaskUartPntPer3S(void);

//UART接收内容检查
void TaskUartChk(void);

//LED闪烁任务，每500ms（50ticks）执行一次
void TaskLedFlash(void);

//OLED刷新任务，每200ms（20ticks）执行一次
void TaskOledFlash(void);

//按键检测逻辑，每20ms执行一次
void TaskKeyCheck(void);

//蜂鸣器控制任务，每100ms执行一次
void TaskBuzzControl(void);

//创建任务配置
schedule_task_t schedule_task[] =
{   //task_1(函数名) 20 (执行周期) 20(计时器) 0(任务执行标志)
    {TaskUartPntPer1S, 100, 2, 0},
    {TaskUartPntPer3S, 300, 4, 0},
    {TaskBuzzControl, 10,  6, 0},
    {TaskUartChk, 10,  6, 0},
    {TaskLedFlash, 50,  8, 0},
    {TaskKeyCheck, 2,  1, 0},
    {TaskOledFlash, 20,  10, 0},
};

//按键对应对象的状态
unsigned char KeyStatus = 0;
//拨码开关状态
unsigned char SwStatus = 0;

//按键触发值
unsigned char KeyTrig = 0;
//按键持续值
unsigned char KeyCont = 0;

//按键检测逻辑，每20ms执行一次
void TaskKeyCheck(void)
{
    unsigned char Key;
    //读取拨码开关状态
    SwStatus = getSwitch();

    //按键检测逻辑
    Key = getButton();
    KeyTrig = Key & (Key ^ KeyCont);
    KeyCont = Key;

    KeyStatus ^= KeyTrig;
}

unsigned char KeySave = 0;
unsigned char SwSave = 0;
//OLED刷新任务，每200ms（20ticks）执行一次
void TaskOledFlash(void)
{
    if((KeySave != KeyStatus) ||(SwSave != SwStatus))
    {
        //刷新OLED屏幕上的按键字符状态
        ShowKey(KeyStatus, SwStatus);
        KeySave = KeyStatus;
        SwSave = SwStatus;
    }
}

//蜂鸣器控制任务，每100ms执行一次
void TaskBuzzControl(void)
{
    //当Key1长按时，蜂鸣器响
    BuzzEn(KeyCont & 0x02);
}

//每秒UART打印任务
void TaskUartPntPer1S(void)
{
    static int cnt = 0;
    printf("This is Task 1 @ %d Sec from DY-HardwareLab\r\n", cnt);
    cnt ++;
}

void TaskUartPntPer3S(void)
{
    static int cnt = 0;
    printf("This is Task 2 @ %d Sec from DY-HardwareLab\r\n", cnt);
    cnt +=3;
}

void TaskUartChk(void)
{
    unsigned short len;
    unsigned short n;
    if(UART_RX_STA & 0x8000)    //check the received finish flag
    {
        len = UART_RX_STA & 0x3FFF; //Got the Length of the received
        printf("\r\nYou Send:\t\r\n");
        //Send the received Line
        for(n=0; n<len; n++)
            DL_UART_Main_transmitDataBlocking(UART_0_INST, LineBuffer[n]);

        printf("\r\n from DY-HardwareLab\r\n");
        UART_RX_STA = 0;    //Clear the Flag of the Received
    }
}

//LED闪烁任务，每500ms（50ticks）执行一次
void TaskLedFlash(void)
{
    static unsigned char val = 0;
    //LED_1 翻转一次
    LedCntl(val);
    val ++;
}


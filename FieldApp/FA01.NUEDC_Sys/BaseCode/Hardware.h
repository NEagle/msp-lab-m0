/*
 * Hardware.h
 *
 *  Created on: 2024年6月27日
 *      Author: junying
 */

#ifndef HARDWARE_H_
#define HARDWARE_H_

#include "ti_msp_dl_config.h"

//LED控制函数
//输入参数：val | bit0 - 红色LED   | bit1 - 白色LED  | bit2 - 黄色LED  |
//  各bit 1 - 点亮 | 0 - 熄灭    |
void LedCntl(unsigned char val);

//LED翻转函数
//输入参数：val | bit0 - 红色LED   | bit1 - 白色LED  | bit2 - 黄色LED  |
//  各bit 1 - 翻转 | 0 - 不变    |
void LedToggle(unsigned char val);

//蜂鸣器控制函数
//输入参数：val | 非零 - 鸣叫 | 0 - 不响    |
void BuzzEn(unsigned char val);

//获取按键状态函数
//返回参数： | bit0 - BSL按键  | bit1 - Key1按键 | bit2 - Key2按键 | bit3 - Key3按键 |
//  各bit 1 - 有按键 | 0 - 没有按键 |
unsigned char getButton(void);

//获取拨码开关状态函数
//返回参数： | bit0 - Key4拨码开关  | bit1 - Key5拨码开关 |
//  各bit 1 - ON | 0 - OFF   |
unsigned char getSwitch(void);

//按键信息显示在OLED上的函数
//输入参数：
//  KeyVal - 轻触开关信息 | bit0 - BSL按键  | bit1 - Key1按键 | bit2 - Key2按键 | bit3 - Key3按键 |
//  SwVal - 拨码开关信息  | bit0 - Key4拨码开关  | bit1 - Key5拨码开关 |
void ShowKey(unsigned char KeyVal, unsigned char SwVal);

//板级系统初始化函数
void BoardInit(void);

#endif /* HARDWARE_H_ */

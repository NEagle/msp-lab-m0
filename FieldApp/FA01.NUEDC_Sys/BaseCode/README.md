## Example Summary

Toggles One GPIO pin using HW toggle register, while several task print msg to UART.

## Peripherals & Pin Assignments

| Peripheral | Pin | Function |
| --- | --- | --- |
| LED-R | PB6 | RED-LED |
| LED-Y | PA8 | Yellow-LED |
| LED-W | PA9 | White-LED |
| Switch-5 | PB3 | Switch 5 |
| Switch-4 | PB2 | Switch 4 |
| BUTTON-3 | PA7 | Button 3 |
| BUTTON-2 | PA28 | Button 2 |
| BUTTON-1 | PA3 | Button 1 |
| BUTTON-0 | PA18 | Button 0 & BSL Key |
| BUZZ | PA2 | BUZZ Enable |
| SDA | PA0 | SDA Line of OLED |
| SCL | PA2 | SCL Line of OLED |
| TXD0 | PA10 | TXD of UART0 & BSL TXD |
| RXD0 | PA11 | RXD of UART0 & BSL RXD |
| SYSCTL | --- | --- |
| EVENT | --- | --- |

## Example Usage
Compile, load and run the example.  
实现功能：
```
    1. 轻触开关将控制OLED对应字符正片显示或者负片显示  
    2. 拨码开关状态直接显示在OLED（ON状态为负片显示）  
    3. KEY1按下时，则蜂鸣器响  
    4. 三个LED按照0.5秒组合闪烁  
    5. 整体程序框架采用时间片轮询的方案  
        5.1 时间片颗粒度为10ms  
        5.2 总共有7个任务，其中两个为不同时间间隔打印UART的测试任务  
        5.3 按键检测采用Trig算法，目前使用Trig作为切换状态，Cont作为蜂鸣器控制  
```

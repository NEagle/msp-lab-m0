/*
 * Hardware.c
 *
 *  Created on: 2024年6月27日
 *      Author: junying
 */

#include "Hardware.h"
#include "oled.h"
#include "printf.h"

//LED控制函数
//输入参数：val | bit0 - 红色LED   | bit1 - 白色LED  | bit2 - 黄色LED  |
//  各bit 1 - 点亮 | 0 - 熄灭    |
void LedCntl(unsigned char val)
{
    if(val & 0x1)
        DL_GPIO_clearPins(LEDS_RED_PORT, LEDS_RED_PIN );
    else
        DL_GPIO_setPins(LEDS_RED_PORT, LEDS_RED_PIN );

    if(val & 0x2)
        DL_GPIO_clearPins(LEDS_WHITE_PORT, LEDS_WHITE_PIN );
    else
        DL_GPIO_setPins(LEDS_WHITE_PORT, LEDS_WHITE_PIN );

    if(val & 0x4)
        DL_GPIO_clearPins(LEDS_YELLOW_PORT, LEDS_YELLOW_PIN );
    else
        DL_GPIO_setPins(LEDS_YELLOW_PORT, LEDS_YELLOW_PIN );

}

//LED翻转函数
//输入参数：val | bit0 - 红色LED   | bit1 - 白色LED  | bit2 - 黄色LED  |
//  各bit 1 - 翻转 | 0 - 不变    |
void LedToggle(unsigned char val)
{
    if(val & 0x1)
        DL_GPIO_togglePins(LEDS_RED_PORT, LEDS_RED_PIN );

    if(val & 0x2)
        DL_GPIO_togglePins(LEDS_WHITE_PORT, LEDS_WHITE_PIN );

    if(val & 0x4)
        DL_GPIO_togglePins(LEDS_YELLOW_PORT, LEDS_YELLOW_PIN );

}

//蜂鸣器控制函数
//输入参数：val | 非零 - 鸣叫 | 0 - 不响    |
void BuzzEn(unsigned char val)
{
    if(val)
        DL_GPIO_setPins(BUZZ_PORT, BUZZ_EN_PIN );
    else
        DL_GPIO_clearPins(BUZZ_PORT, BUZZ_EN_PIN );
}

//获取按键状态函数
//返回参数： | bit0 - BSL按键  | bit1 - Key1按键 | bit2 - Key2按键 | bit3 - Key3按键 |
//  各bit 1 - 有按键 | 0 - 没有按键 |
unsigned char getButton(void)
{
    unsigned char val = 0;
    if(DL_GPIO_readPins(BUTTON_PORT, BUTTON_BSL_PIN))
        val |= 0x01;

    if(!DL_GPIO_readPins(BUTTON_PORT, BUTTON_KEY1_PIN))
        val |= 0x02;

    if(!DL_GPIO_readPins(BUTTON_PORT, BUTTON_KEY2_PIN))
        val |= 0x04;

    if(!DL_GPIO_readPins(BUTTON_PORT, BUTTON_KEY3_PIN))
        val |= 0x08;

    return val;
}

//获取拨码开关状态函数
//返回参数： | bit0 - Key4拨码开关  | bit1 - Key5拨码开关 |
//  各bit 1 - ON | 0 - OFF   |
unsigned char getSwitch(void)
{
    unsigned char val = 0;
    if(!DL_GPIO_readPins(SWITCH_PORT, SWITCH_KEY4_PIN))
        val |= 0x01;

    if(!DL_GPIO_readPins(SWITCH_PORT, SWITCH_KEY5_PIN))
        val |= 0x02;

    return val;
}

void Panel_Init(void)
{
    OLED_Init();
    OLED_ColorTurn(0);
    OLED_DisplayTurn(0);

    OLED_ShowChinese(0,0,0,16,1);//硬
    OLED_ShowChinese(18,0,1,16,1);//件
    OLED_ShowChinese(36,0,2,16,1);//创
    OLED_ShowChinese(54,0,3,16,1);//客
    OLED_ShowChinese(72,0,4,16,1);//实
    OLED_ShowChinese(90,0,5,16,1);//验
    OLED_ShowChinese(108,0,6,16,1);//室
    OLED_ShowString(8,16,(u8 *)" HardwareFab ",16,1);
    OLED_ShowString(0,32,(u8 *)"Key2  Key3  SW5 ",16,1);
    OLED_ShowString(0,48,(u8 *)"Key0  Key1  SW4 ",16,1);
    OLED_Refresh();
}

//按键信息显示在OLED上的函数
//输入参数：
//  KeyVal - 轻触开关信息 | bit0 - BSL按键  | bit1 - Key1按键 | bit2 - Key2按键 | bit3 - Key3按键 |
//  SwVal - 拨码开关信息  | bit0 - Key4拨码开关  | bit1 - Key5拨码开关 |
void ShowKey(unsigned char KeyVal, unsigned char SwVal)
{
    if(KeyVal & 0x01)
        OLED_ShowString(0,48,(u8*)"Key0", 16, 0);
    else
        OLED_ShowString(0,48,(u8*)"Key0", 16, 1);

    if(KeyVal & 0x02)
        OLED_ShowString(48,48,(u8*)"Key1", 16, 0);
    else
        OLED_ShowString(48,48,(u8*)"Key1", 16, 1);

    if(KeyVal & 0x04)
        OLED_ShowString(0,32,(u8*)"Key2", 16, 0);
    else
        OLED_ShowString(0,32,(u8*)"Key2", 16, 1);

    if(KeyVal & 0x08)
        OLED_ShowString(48,32,(u8*)"Key3", 16, 0);
    else
        OLED_ShowString(48,32,(u8*)"Key3", 16, 1);

    if(SwVal & 0x01)
        OLED_ShowString(96,48,(u8*)"SW4", 16, 0);
    else
        OLED_ShowString(96,48,(u8*)"SW4", 16, 1);

    if(SwVal & 0x02)
        OLED_ShowString(96,32,(u8*)"SW5", 16, 0);
    else
        OLED_ShowString(96,32,(u8*)"SW5", 16, 1);

    OLED_Refresh();
}

void BoardInit(void)
{
    Panel_Init();
}

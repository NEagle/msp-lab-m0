# 现场应用系统
本目录汇聚基于TI M0+各类现场应用的软硬件系统  

## FA01.电赛最小系统板[目录](https://gitee.com/NEagle/msp-lab-m0/tree/master/FieldApp/FA01.NUEDC_Sys/)
基于MSPM0G350x的最小系统板，支持电赛应用开发  
### 硬件资料
[原理图](https://gitee.com/NEagle/msp-lab-m0/raw/master/FieldApp/FA01.NUEDC_Sys/Hardware/SCH_%E7%94%B5%E8%B5%9B%E6%9C%80%E5%B0%8F%E7%B3%BB%E7%BB%9F.pdf)  
[丝印图](https://gitee.com/NEagle/msp-lab-m0/raw/master/FieldApp/FA01.NUEDC_Sys/Hardware/Top.png)  

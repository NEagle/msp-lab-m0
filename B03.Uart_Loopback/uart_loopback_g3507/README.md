## Example Summary

Toggles One GPIO pin using HW toggle register.

## Peripherals & Pin Assignments

| Peripheral | Pin | Function |
| --- | --- | --- |
| GPIOA | PA0 | Standard Output-LED |
| GPIOA | PA11 | UART RX on CoreBoard |
| GPIOA | PA10 | UART TX on CoreBoard |
| SYSCTL | --- | --- |
| EVENT | --- | --- |

## Example Usage
Compile, load and run the example.  
LED on CoreBoard will toggle, and UART Loop Back from PC super-terminal.  

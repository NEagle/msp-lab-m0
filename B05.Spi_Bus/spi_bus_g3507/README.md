## Example Summary

SPI Bus Example with SPI FLASH Accessing.

## Peripherals & Pin Assignments

| Peripheral | Pin | Function |
| --- | --- | --- |
| GPIOA | PA0 | Standard Output-LED |
| GPIOA | PA11 | UART RX on CoreBoard |
| GPIOA | PA10 | UART TX on CoreBoard |
| GPIOA | PA15 | SPI FLASH CS PIN |
| GPIOA | PA18 | SPI FLASH MOSI PIN |
| GPIOA | PA16 | SPI FLASH MISO PIN |
| GPIOA | PA17 | SPI FLASH SCK PIN |
| SYSCTL | --- | --- |
| EVENT | --- | --- |

## Example Usage
Compile, load and run the example.  
Run FLASH Erase/Program/Read Testing.  

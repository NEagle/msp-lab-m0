## Example Summary

SPI Bus Example with SPI FLASH Accessing.

## Peripherals & Pin Assignments

| Peripheral | Pin | Function |
| --- | --- | --- |
| GPIOA | PA0 | Standard Output-LED |
| GPIOA | PA22 | UART RX on CoreBoard |
| GPIOA | PA23 | UART TX on CoreBoard |
| GPIOA | PA2 | SPI FLASH CS PIN |
| GPIOA | PA5 | SPI FLASH MOSI PIN |
| GPIOA | PA4 | SPI FLASH MISO PIN |
| GPIOA | PA6 | SPI FLASH SCK PIN |
| SYSCTL | --- | --- |
| EVENT | --- | --- |

## Example Usage
Compile, load and run the example.  
Run FLASH Erase/Program/Read Testing.  

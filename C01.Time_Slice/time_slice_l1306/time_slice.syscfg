/**
 * These arguments were used when this file was generated. They will be automatically applied on subsequent loads
 * via the GUI or CLI. Run CLI with '--help' for additional information on how to override these arguments.
 * @cliArgs --device "MSPM0L130X" --package "VQFN-32(RHB)" --part "Default" --product "mspm0_sdk@1.20.01.06"
 * @versions {"tool":"1.18.0+3266"}
 */

/**
 * Import the modules used in this configuration.
 */
const GPIO    = scripting.addModule("/ti/driverlib/GPIO", {}, false);
const GPIO1   = GPIO.addInstance();
const SYSCTL  = scripting.addModule("/ti/driverlib/SYSCTL");
const SYSTICK = scripting.addModule("/ti/driverlib/SYSTICK");
const UART    = scripting.addModule("/ti/driverlib/UART", {}, false);
const UART1   = UART.addInstance();

/**
 * Write custom configuration values to the imported modules.
 */
GPIO1.$name                         = "GPIO_LEDS";
GPIO1.port                          = "PORTA";
GPIO1.associatedPins[0].$name       = "USER_LED_1";
GPIO1.associatedPins[0].assignedPin = "0";

const Board = scripting.addModule("/ti/driverlib/Board", {}, false);

SYSCTL.clockTreeEn = true;

SYSTICK.periodEnable    = true;
SYSTICK.interruptEnable = true;
SYSTICK.period          = 320000;
SYSTICK.systickEnable   = true;

UART1.$name                    = "UART_0";
UART1.targetBaudRate           = 115200;
UART1.rxFifoThreshold          = "DL_UART_RX_FIFO_LEVEL_ONE_ENTRY";
UART1.enableDMARX              = false;
UART1.enableDMATX              = false;
UART1.enabledInterrupts        = ["RX"];
UART1.peripheral.$assign       = "UART0";
UART1.peripheral.rxPin.$assign = "PA22";
UART1.peripheral.txPin.$assign = "PA23";
UART1.txPinConfig.$name        = "ti_driverlib_gpio_GPIOPinGeneric0";
UART1.rxPinConfig.$name        = "ti_driverlib_gpio_GPIOPinGeneric1";

/**
 * Pinmux solution for unlocked pins/peripherals. This ensures that minor changes to the automatic solver in a future
 * version of the tool will not impact the pinmux you originally saw.  These lines can be completely deleted in order to
 * re-solve from scratch.
 */
GPIO1.associatedPins[0].pin.$suggestSolution = "PA0";
Board.peripheral.$suggestSolution            = "DEBUGSS";
Board.peripheral.swclkPin.$suggestSolution   = "PA20";
Board.peripheral.swdioPin.$suggestSolution   = "PA19";

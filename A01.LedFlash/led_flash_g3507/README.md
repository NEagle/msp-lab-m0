## Example Summary

Toggles Four GPIO pins to control LED2(RGB LED) and LED1.

## Peripherals & Pin Assignments

| Peripheral | Pin | Function |
| --- | --- | --- |
| GPIOA | PA0 | LED1 |
| GPIOB | PA26 | LED2-BLUE |
| GPIOB | PA27 | LED2-GREEN |
| GPIOB | PA22 | LED2-BLUE |
| SYSCTL | --- | --- |
| EVENT | --- | --- |

## Example Usage
Compile, load and run the example.
LEDs on CoreBoard will toggle.

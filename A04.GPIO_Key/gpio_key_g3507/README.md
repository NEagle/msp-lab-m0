## Example Summary

Toggles One Led while Pressed once Key.

## Peripherals & Pin Assignments

| Peripheral | Pin | Function |
| --- | --- | --- |
| GPIOA | PA0 | Standard Output-LED |
| GPIOA | PA14 | Connect to One Key |
| SYSCTL | --- | --- |
| EVENT | --- | --- |

## Example Usage
Compile, load and run the example.
LED will toggle while One Press on Keys.

## Keil工程脚本支持
本目录提供keil工程所需要的自动化编译工具：  
### 1、读取*.syscfg文件，生成c文件的syscfg.bat批命令  
    a、需要注意修改你系统中的SysConfig软件的软件版本，并修改该bat文件中的安装目录  
        如你的系统在c:\ti\sysconfig_1.20.0\目录下安装的软件，需要将syscfg.bat的第3行  
          "C:\ti\sysconfig_1.18.0\sysconfig_cli.bat"  
          修改为"C:\ti\sysconfig_1.20.0\sysconfig_cli.bat"  
    b、需要注意修改你系统的MSPM0的SDK版本及目录  
        如你系统中在c:\mspm0_sdk_2_00_01_00\目录下安装对应的SDK，则需要将syscfg.bat的第22行    
          "C:\ti\mspm0_sdk_1_20_01_06\.metadata\product.json"  
          修改为"C:\ti\mspm0_sdk_2_00_01_00\.metadata\product.json"  
### 2、工程编译生成*.txt目标文件的gentxt.bat批命令 [网盘地址](https://pan.baidu.com/s/1cfELuMFI8mVOPzCdh4K_Ug?pwd=vegw)  
    a、需要注意修改你系统中的CCS版本及目录  
    b、如你的系统在c:\ti\ccs1271\目录安装的CCS版本，则需要将gentxt.bat的第3行  
       "C:\ti\ccs1250\ccs\tools\compiler\ti-cgt-armllvm_3.2.0.LTS\bin\tiarmhex.exe" 修改为  
       "C:\ti\ccs1271\ccs\tools\compiler\ti-cgt-armllvm_3.2.0.LTS\bin\tiarmhex.exe"  
    c、若你的系统没有安装ccs，则可以使用网盘上的/Tools/TXT生成工具 目录上的转换工具tiarmhex.exe文件（下载到你的目录，并将相应目录的地址修改至gentxt.bat的第三行）  
### 3、各工程目录下的*.uvprojx文件中需要修改三个与sdk版本及目录有关的条目  
    a、二进制库文件driverlib.a的目录需要  
    从 C:/ti/mspm0_sdk_1_20_01_06/source/ti/driverlib/lib/keil/m0p/mspm0g1x0x_g3x0x/driverlib.a  
    修改为 C:/ti/mspm0_sdk_2_00_01_00/source/ti/driverlib/lib/keil/m0p/mspm0g1x0x_g3x0x/driverlib.a  
    b、工程设置中C/C++页的Include Path的两个目录需要修改  
        i/ C:\ti\mspm0_sdk_1_20_01_06/source/third_party/CMSIS/Core/Include  
          修改为 C:\ti\mspm0_sdk_2_00_01_00/source/third_party/CMSIS/Core/Include  
        ii/ C:\ti\mspm0_sdk_1_20_01_06\source  
          修改为 C:\ti\mspm0_sdk_2_00_01_00\source  

## Example Summary

Toggles One GPIO pin using HW toggle register.

## Peripherals & Pin Assignments

| Peripheral | Pin | Function |
| --- | --- | --- |
| GPIOA | PA0 | Standard Output-LED on Core board |
| GPIOA | PA14 | Connect to One LED Pin on Base board, such as LED on J10 |
| SYSCTL | --- | --- |
| EVENT | --- | --- |

## Example Usage
Compile, load and run the example.
LED on CoreBoard will toggle, and LED on Baseboard will change brightness.

## Example Summary

Control Common Cathode Digital Tube Display from 0 - F per Sencond.

## Peripherals & Pin Assignments

| Peripheral | Pin | Function |
| --- | --- | --- |
| GPIOA | PA0 | Standard Output-LED on Core Board |
| GPIOA | PA2 | Connect to CC-Tube Input A with Dupont Line |
| GPIOA | PA3 | Connect to CC-Tube Input B with Dupont Line |
| GPIOA | PA4 | Connect to CC-Tube Input C with Dupont Line |
| GPIOA | PA5 | Connect to CC-Tube Input D with Dupont Line |
| GPIOA | PA6 | Connect to CC-Tube Input E with Dupont Line |
| GPIOA | PA7 | Connect to CC-Tube Input F with Dupont Line |
| GPIOA | PA8 | Connect to CC-Tube Input G with Dupont Line |
| GPIOA | PA9 | Connect to CC-Tube Input DP with Dupont Line |
| SYSCTL | --- | --- |
| EVENT | --- | --- |
| DEBUGSS | PA20 | Debug Clock |
| DEBUGSS | PA19 | Debug Data In Out |

## Example Usage
Compile, load and run the example.
0 ~ F will Display on CC-Tube per second.
LED on Core Board will toggled per second.


## Example Summary

Capture One Signal - Measurement the Period & Duty.

## Peripherals & Pin Assignments

| Peripheral | Pin | Function |
| --- | --- | --- |
| GPIOA | PA0 | Standard Output-LED on Core board |
| GPIOA | PA14 | PWM signal Output for measurement by capture |
| GPIOA | PA12 | Signal Capture Input Pin |
| SYSCTL | --- | --- |
| EVENT | --- | --- |

## Example Usage
Compile, load and run the example.
Print Out the Measurement Value of Period & Duty from UART Port.
